/*
* Copyright (c) 2023 Huawei Device Co., Ltd.
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef OH_VALUE_OBJECT_H
#define OH_VALUE_OBJECT_H

/**
 * @addtogroup RDB
 * @{
 *
 * @brief The relational database (RDB) store manages data based on relational models.
 * A complete set of mechanisms for managing local databases is provided based on the underlying SQLite.
 * To satisfy different needs in complicated scenarios, the RDB module provides a series of methods for performing
 * operations such as adding, deleting, modifying, and querying data, and supports direct execution of SQL statements.
 *
 * @syscap SystemCapability.DistributedDataManager.RelationalStore.Core
 * @since 10
 */

/**
 * @file oh_value_object.h
 *
 * @brief Provides data conversion methods.
 *
 * @since 10
 */

#include <cstdint>
#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Defines the allowed data field types.
 *
 * @since 10
 */
typedef struct OH_VObject {
    /** Unique identifier of the OH_VObject struct. */
    int64_t id;

    /**
     * @brief Converts an int64 value or array into a value of the {@link OH_VObject} type.
     *
     * @param valueObject Indicates the pointer to the {@link OH_VObject} instance.
     * @param value Indicates the pointer to the value or array to convert.
     * @param count Indicates the number or length of the parameters to convert.
     * If <b>value</b> points to a single parameter, <b>count</b> is <b>1</b>.
     * If <b>value</b> points to an array, <b>count</b> specifies the length of the array.
     * @return Returns the operation result. If the operation fails, an error code is returned.
     * @see OH_VObject.
     * @since 10
     */
    int (*putInt64)(OH_VObject *valueObject, int64_t *value, uint32_t count);

    /**
     * @brief Converts a double value or array into a value of the {@link OH_VObject} type.
     *
     * @param valueObject Indicates the pointer to the {@link OH_VObject} instance.
     * @param value Indicates the pointer to the double value or array to convert.
     * @param count Indicates the number or length of the parameters to convert.
     * If <b>value</b> points to a single parameter, <b>count</b> is <b>1</b>.
     * If <b>value</b> points to an array, <b>count</b> specifies the length of the array.
     * @return Returns the operation result. If the operation fails, an error code is returned.
     * @see OH_VObject.
     * @since 10
     */
    int (*putDouble)(OH_VObject *valueObject, double *value, uint32_t count);

    /**
     * @brief Converts a character array of the char type into a value of the {@link OH_VObject} type.
     *
     * @param valueObject Indicates the pointer to the {@link OH_VObject} instance.
     * @param value Indicates the pointer to the character array to convert.
     * @return Returns the operation result. If the operation fails, an error code is returned.
     * @see OH_VObject.
     * @since 10
     */
    int (*putText)(OH_VObject *valueObject, const char *value);

    /**
     * @brief Converts a string array of the char type into a value of the {@link OH_VObject} type.
     *
     * @param valueObject Indicates the pointer to the {@link OH_VObject} instance.
     * @param value Indicates the pointer to the string array to convert.
     * @param count Indicates the length of the string array to convert.
     * @return Returns the operation result. If the operation fails, an error code is returned.
     * @see OH_VObject.
     * @since 10
     */
    int (*putTexts)(OH_VObject *valueObject, const char **value, uint32_t count);

    /**
     * @brief Destroys a {@link OH_VObject} instance and reclaims the memory occupied.
     *
     * @param valueObject Indicates the pointer to the {@link OH_VObject} instance.
     * @return Returns the operation result. If the operation fails, an error code is returned.
     * @see OH_VObject.
     * @since 10
     */
    int (*destroyValueObject)(OH_VObject *valueObject);
} OH_VObject;

#ifdef __cplusplus
};
#endif

#endif // OH_VALUE_OBJECT_H
