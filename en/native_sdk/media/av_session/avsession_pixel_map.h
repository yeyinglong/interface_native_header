/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef OHOS_AVSESSION_PIXEL_MAP_H
#define OHOS_AVSESSION_PIXEL_MAP_H

/**
 * @addtogroup avsession
 * @{
 *
 * @brief Provides APIs for audio and video (AV) media control.
 *
 * The APIs can be used to create, manage, and control media sessions.
 *
 * @syscap SystemCapability.Multimedia.AVSession.Core
 * @since 9
 * @version 1.0
 */

/**
 * @file avsession_pixel_map.h
 *
 * @brief Declares the APIs for obtaining and setting image data and image information.
 *
 * @since 9
 * @version 1.0
 */

#include <vector>
#include "parcel.h"

#if !defined(WINDOWS_PLATFORM) and !defined(MAC_PLATFORM) and !defined(IOS_PLATFORM)
#include <malloc.h>
#endif

namespace OHOS::AVSession {
/** The initial container size is 160 KB. */
constexpr size_t DEFAULT_BUFFER_SIZE = 160 * 1024;

/**
 * @brief Implements APIs for obtaining and setting image data and image information.
 *
 */
class AVSessionPixelMap : public Parcelable {
public:
    AVSessionPixelMap() = default;
    ~AVSessionPixelMap()
    {
#if !defined(WINDOWS_PLATFORM) and !defined(MAC_PLATFORM) and !defined(IOS_PLATFORM)
#if defined(__BIONIC__)
        mallopt(M_PURGE, 0);
#endif
#endif
    }

    /**
     * @brief Marshals image data and image information into a {@link Parcel} object.
     *
     * @param data Indicates the pointer to the {@link Parcel} object for marshalling.
     * @return Returns <b>true</b> if the operation is successful; returns <b>false</b> otherwise.
     * @see Unmarshalling
     * @since 9
     * @version 1.0
     */
    bool Marshalling(Parcel &data) const override;

    /**
     * @brief Unmarshals image data and image information from a {@link Parcel} object.
     *
     * @param data Indicates the pointer to the {@link Parcel} object for unmarshalling.
     * @return Returns <b>true</b> if the operation is successful; returns <b>false</b> otherwise.
     * @see Marshalling
     * @since 9
     * @version 1.0
     */
    static AVSessionPixelMap *Unmarshalling(Parcel &data);

    /**
     * @brief Obtains image data.
     *
     * @return Returns the image data.
     * @see SetPixelData
     * @since 9
     * @version 1.0
     */
    std::vector<uint8_t> GetPixelData() const
    {
        return data_;
    }

    /**
     * @brief Sets image data.
     *
     * @param data Indicates the pointer to the image data to set.
     * @see GetPixelData
     * @since 9
     * @version 1.0
     */
    void SetPixelData(const std::vector<uint8_t> &data)
    {
        data_.clear();
        data_ = data;
    }

    /**
     * @brief Obtains image information.
     *
     * @return Returns the image information.
     * @see SetImageInfo
     * @since 9
     * @version 1.0
     */
    std::vector<uint8_t> GetImageInfo() const
    {
        return imageInfo_;
    }

    /**
     * @brief Sets image information.
     *
     * @param imageInfo Indicates the pointer to the image information to set.
     * @see GetImageInfo
     * @since 9
     * @version 1.0
     */
    void SetImageInfo(const std::vector<uint8_t> &imageInfo)
    {
        imageInfo_.clear();
        imageInfo_ = imageInfo;
    }

private:
    /** Image data. */
    std::vector<uint8_t> data_ = std::vector<uint8_t>(DEFAULT_BUFFER_SIZE);
    /** Image information. */
    std::vector<uint8_t> imageInfo_;
};
} // namespace OHOS::AVSession
/** @} */
#endif // OHOS_AVSESSION_PIXEL_MAP_H
