/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Display
 * @{
 *
 * @brief Defines driver interfaces of the display module.
 *
 * This module provides driver interfaces for upper-layer graphics services, including layer management, device control, and display buffer management.
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file DisplayComposerType.idl
 *
 * @brief Declares the data types used by the interfaces related to display composer operations.
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @brief Defines the package path of the display module interfaces.
 *
 * @since 3.2
 * @version 1.0
 */
package ohos.hdi.display.composer.v1_0;
sequenceable OHOS.HDI.Display.BufferHandleParcelable;
sequenceable OHOS.HDI.Display.HdifdParcelable;

enum DispCmd {
    /* Request commands */
    REQUEST_CMD_PREPAREDISPLAYLAYERS = 256,
    REQUEST_CMD_SETDISPLAYCLIENTBUFFER = 257,
    REQUEST_CMD_SETDISPLAYCLIENTDAMAGE = 258,
    REQUEST_CMD_COMMIT = 259,
    REQUEST_CMD_SETLAYERALPHA = 260,
    REQUEST_CMD_SETLAYERPOSITION = 261,
    REQUEST_CMD_SETLAYERCROP = 262,
    REQUEST_CMD_SETLAYERZORDER = 263,
    REQUEST_CMD_SETLAYERPREMULTI = 264,
    REQUEST_CMD_SETTRANSFORMMODE = 265,
    REQUEST_CMD_SETLAYERDIRTYREGION = 266,
    REQUEST_CMD_SETLAYERVISIBLEREGION = 267,
    REQUEST_CMD_SETLAYERBUFFER = 268,
    REQUEST_CMD_SETLAYERCOMPOSITIONTYPE = 269,
    REQUEST_CMD_SETLAYERBLENDTYPE = 270,
    REQUEST_CMD_SETLAYERVISIBLE = 271,
    /* Reply commands */
    REPLY_CMD_SETERROR = 1024,
    REPLY_CMD_PREPAREDISPLAYLAYERS = 1025,
    REPLY_CMD_COMMIT = 1026,
    /* Package control commands */
    CONTROL_CMD_REQUEST_BEGIN = 8192,
    CONTROL_CMD_REPLY_BEGIN = 8193,
    CONTROL_CMD_REQUEST_END = 8194,
    CONTROL_CMD_REPLY_END = 8195,
};

/**
 * @brief Enumerates the display interface types.
 *
 */
enum InterfaceType {
    DISP_INTF_HDMI = 0,           /**< HDMI interface */
    DISP_INTF_LCD = 1,            /**< LCD interface */
    DISP_INTF_BT1120 = 2,         /**< BT1120 interface */
    DISP_INTF_BT656 = 3,          /**< BT656 interface */
    DISP_INTF_YPBPR = 4,          /**< YPBPR interface */
    DISP_INTF_RGB = 5,            /**< RGB interface */
    DISP_INTF_CVBS = 6,           /**< CVBS interface */
    DISP_INTF_SVIDEO = 7,         /**< SVIDEO interface */
    DISP_INTF_VGA = 8,            /**< VGA interface */
    DISP_INTF_MIPI = 9,           /**< MIPI interface */
    DISP_INTF_PANEL = 10,         /**< PANEL interface */
    DISP_INTF_BUTT = 11,          /**< Invalid value, used for default initialization */
};

/**
 * @brief Enumerates the return values of the interfaces.
 *
 */
enum DispErrCode {
    DISPLAY_SUCCESS = 0,           /**< Success */
    DISPLAY_FAILURE = -1,          /**< Failure */
    DISPLAY_FD_ERR = -2,           /**< File descriptor (FD) error */
    DISPLAY_PARAM_ERR = -3,        /**< Parameter error */
    DISPLAY_NULL_PTR = -4,         /**< Null pointer */
    DISPLAY_NOT_SUPPORT = -5,      /**< Unsupported feature */
    DISPLAY_NOMEM = -6,            /**< Insufficient memory */
    DISPLAY_SYS_BUSY = -7,         /**< System busy */
    DISPLAY_NOT_PERM = -8,         /**< Forbidden operation */
};

/**
 * @brief Enumerates the layer types.
 *
 */
enum LayerType {
    LAYER_TYPE_GRAPHIC = 0,        /**< Graphics layer */
    LAYER_TYPE_OVERLAY = 1,        /**< Overlay layer */
    LAYER_TYPE_SIDEBAND = 2,       /**< Sideband layer */
    LAYER_TYPE_CURSOR = 3,         /**< Cursor layer */
    LAYER_TYPE_BUTT = 4,           /**< Invalid value */
};

enum BufferUsage {
    HBM_USE_CPU_READ = ( 1 << 0 ),
    HBM_USE_CPU_WRITE = ( 1 << 1 ),
    HBM_USE_MEM_MMZ = ( 1 << 2 ),
    HBM_USE_MEM_DMA = ( 1 << 3 ),
    HBM_USE_MEM_SHARE = ( 1 << 4 ),
    HBM_USE_MEM_MMZ_Cache = ( 1 << 5 ),
    HBM_USE_MEM_FB = ( 1 << 6 ),
    HBM_USE_ASSIGN_SIZE = ( 1 << 7 ),
};

/**
 * @brief Enumerates the pixel formats.
 *
 */
enum PixelFormat {
    PIXEL_FMT_CLUT8 = 0,                      /**< CLUT8 format */
    PIXEL_FMT_CLUT1 = 1,                      /**< CLUT1 format */
    PIXEL_FMT_CLUT4 = 2,                      /**< CLUT4 format */
    PIXEL_FMT_RGB_565 = 3,                    /**< RGB565 format */
    PIXEL_FMT_RGBA_5658 = 4,                  /**< RGBA5658 format */
    PIXEL_FMT_RGBX_4444 = 5,                  /**< RGBX4444 format */
    PIXEL_FMT_RGBA_4444 = 6,                  /**< RGBA4444 format */
    PIXEL_FMT_RGB_444 = 7,                    /**< RGB444 format */
    PIXEL_FMT_RGBX_5551 = 8,                  /**< RGBX5551 format */
    PIXEL_FMT_RGBA_5551 = 9,                  /**< RGBA5551 format */
    PIXEL_FMT_RGB_555 = 10,                   /**< RGB555 format */
    PIXEL_FMT_RGBX_8888 = 11,                 /**< RGBX8888 format */
    PIXEL_FMT_RGBA_8888 = 12,                 /**< RGBA8888 format */
    PIXEL_FMT_RGB_888 = 13,                   /**< RGB888 format */
    PIXEL_FMT_BGR_565 = 14,                   /**< BGR565 format */
    PIXEL_FMT_BGRX_4444 = 15,                 /**< BGRX4444 format */
    PIXEL_FMT_BGRA_4444 = 16,                 /**< BGRA4444 format */
    PIXEL_FMT_BGRX_5551 = 17,                 /**< BGRX5551 format */
    PIXEL_FMT_BGRA_5551 = 18,                 /**< BGRA5551 format */
    PIXEL_FMT_BGRX_8888 = 19,                 /**< BGRX8888 format */
    PIXEL_FMT_BGRA_8888 = 20,                 /**< BGRA8888 format */
    PIXEL_FMT_YUV_422_I = 21,                 /**< YUV422 interleaved format */
    PIXEL_FMT_YCBCR_422_SP = 22,              /**< YCBCR422 semi-planar format */
    PIXEL_FMT_YCRCB_422_SP = 23,              /**< YCRCB422 semi-planar format */
    PIXEL_FMT_YCBCR_420_SP = 24,              /**< YCBCR420 semi-planar format */
    PIXEL_FMT_YCRCB_420_SP = 25,              /**< YCRCB420 semi-planar format */
    PIXEL_FMT_YCBCR_422_P = 26,               /**< YCBCR422 planar format */
    PIXEL_FMT_YCRCB_422_P = 27,               /**< YCRCB422 planar format */
    PIXEL_FMT_YCBCR_420_P = 28,               /**< YCBCR420 planar format */
    PIXEL_FMT_YCRCB_420_P = 29,               /**< YCRCB420 planar format */
    PIXEL_FMT_YUYV_422_PKG = 30,              /**< YUYV422 packed format */
    PIXEL_FMT_UYVY_422_PKG = 31,              /**< UYVY422 packed format */
    PIXEL_FMT_YVYU_422_PKG = 32,              /**< YVYU422 packed format */
    PIXEL_FMT_VYUY_422_PKG = 33,              /**< VYUY422 packed format */
    PIXEL_FMT_BUTT = 34,                      /**< Invalid value */
};

/**
 * @brief Enumerates the image transform types.
 *
 */
enum TransformType {
    ROTATE_NONE = 0,            /**< No rotation */
    ROTATE_90 = 1,              /**< Rotation by 90 degrees */
    ROTATE_180 = 2,             /**< Rotation by 180 degrees */
    ROTATE_270 = 3,             /**< Rotation by 270 degrees */
    ROTATE_BUTT = 4,            /**< Invalid value */
};

/**
 * @brief Enumerates the image blending types.
 *
 * The system combines images based on the specified blending type during hardware acceleration.
 *
 */
enum BlendType {
    BLEND_NONE = 0,             /**< No blending */
    BLEND_CLEAR = 1,            /**< CLEAR blending */
    BLEND_SRC = 2,              /**< SRC blending */
    BLEND_SRCOVER = 3,          /**< SRC_OVER blending */
    BLEND_DSTOVER = 4,          /**< DST_OVER blending */
    BLEND_SRCIN = 5,            /**< SRC_IN blending */
    BLEND_DSTIN = 6,            /**< DST_IN blending */
    BLEND_SRCOUT = 7,           /**< SRC_OUT blending */
    BLEND_DSTOUT = 8,           /**< DST_OUT blending */
    BLEND_SRCATOP = 9,          /**< SRC_ATOP blending */
    BLEND_DSTATOP = 10,         /**< DST_ATOP blending */
    BLEND_ADD = 11,             /**< ADD blending */
    BLEND_XOR = 12,             /**< XOR blending */
    BLEND_DST = 13,             /**< DST blending */
    BLEND_AKS = 14,             /**< AKS blending */
    BLEND_AKD = 15,             /**< AKD blending */
    BLEND_BUTT = 16,            /**< Invalid value */
};

/**
 * @brief Enumerates the raster operations pipeline (ROP) types supported by hardware acceleration.
 *
 * ROP performs bitwise Boolean operations (including bitwise AND and bitwise OR) on the RGB color and alpha values of the foreground pixel map with those of the background pixel map, and then outputs the result.
 * 
 *
 */
enum RopType {
    ROP_BLACK = 0,                  /**< Black */
    ROP_NOTMERGEPEN = 1,            /**< ~(S2+S1) */
    ROP_MASKNOTPEN = 2,             /**< ~S2&S1 */
    ROP_NOTCOPYPEN = 3,             /**< ~S2 */
    ROP_MASKPENNOT = 4,             /**< S2&~S1 */
    ROP_NOT = 5,                    /**< ~S1 */
    ROP_XORPEN = 6,                 /**< S2^S1 */
    ROP_NOTMASKPEN = 7,             /**< ~(S2&S1) */
    ROP_MASKPEN = 8,                /**< S2&S1 */
    ROP_NOTXORPEN = 9,              /**< ~(S2^S1) */
    ROP_NOP = 10,                   /**< S1 */
    ROP_MERGENOTPEN = 11,           /**< ~S2+S1 */
    ROP_COPYPE = 12,                /**< S2 */
    ROP_MERGEPENNOT = 13,           /**< S2+~S1 */
    ROP_MERGEPEN = 14,              /**< S2+S1 */
    ROP_WHITE = 15,                 /**< White */
    ROP_BUTT = 16,                  /**< Invalid value */
};

/**
 * @brief Enumerates the color key types supported by hardware acceleration.
 *
 */
enum ColorKey {
    CKEY_NONE = 0,               /**< No color key */
    CKEY_SRC = 1,                /**< Source color key */
    CKEY_DST = 2,                /**< Target color key */
    CKEY_BUTT = 3,               /**< Invalid value */
};

/**
 * @brief Enumerates the mirror types supported by hardware acceleration.
 *
 */
enum MirrorType {
    MIRROR_NONE = 0,         /**< No mirror */
    MIRROR_LR = 1,           /**< Left and right mirroring */
    MIRROR_TB = 2,           /**< Top and bottom mirroring */
    MIRROR_BUTT = 3,         /**< Invalid value */
};

/**
 * @brief Enumerates the hot plug types.
 *
 */
enum Connection {
    CON_INVALID = 0,             /**< Invalid value */
    CONNECTED = 1,               /**< Connected */
    DISCONNECTED = 2,            /**< Disconnected */
};

/**
 * @brief Enumerates the display statuses.
 */
enum DispPowerStatus {
    POWER_STATUS_ON = 0,                /**< On */
    POWER_STATUS_STANDBY = 1,           /**< Standby */
    POWER_STATUS_SUSPEND = 2,           /**< Suspended */
    POWER_STATUS_OFF = 3,               /**< Off */
    POWER_STATUS_BUTT = 4,              /**< Default mode */
};

/**
 * @brief Enumerates the composition types of special layers.
 */
enum CompositionType {
    COMPOSITION_CLIENT = 0,       /**< Client composition type, used for CPU or GPU composition */
    COMPOSITION_DEVICE = 1,       /**< Device composition type, used for device composition */
    COMPOSITION_CURSOR = 2,       /**< Cursor composition type, used for cursor composition */
    COMPOSITION_VIDEO = 3,        /**< Video composition type, used for video layer composition */
    COMPOSITION_BUTT = 4,         /**< Invalid value, used for default initialization */
};

/**
 * @brief Enumerates the color gamut types.
 *
 */
enum ColorGamut {
    COLOR_GAMUT_INVALID = -1,              /**< Invalid value */
    COLOR_GAMUT_NATIVE = 0,                /**< Default value */
    COLOR_GAMUT_SATNDARD_BT601 = 1,        /**< Standard BT601 */
    COLOR_GAMUT_STANDARD_BT709 = 2,        /**< Standard BT709 */
    COLOR_GAMUT_DCI_P3 = 3,                /**< DCI P3 */
    COLOR_GAMUT_SRGB = 4,                  /**< SRGB */
    COLOR_GAMUT_ADOBE_RGB = 5,             /**< Adobe RGB */
    COLOR_GAMUT_DISPLAY_P3 = 6,            /**< Display P3 */
    COLOR_GAMUT_BT2020 = 7,                /**< BT.2020 */
    COLOR_GAMUT_BT2100_PQ = 8,             /**< BT.2100 PQ */
    COLOR_GAMUT_BT2100_HLG = 9,            /**< BT.2100 HLG */
    COLOR_GAMUT_DISPLAY_BT2020 = 10,       /**< Display BT.2020 */
};

/**
 * @brief Enumerates the color gamut mapping types.
 *
 */
enum GamutMap {
    GAMUT_MAP_CONSTANT = 0,           /**< Constant */
    GAMUT_MAP_EXPANSION = 1,          /**< Mapping expansion */
    GAMUT_MAP_HDR_CONSTANT = 2,       /**< Constant, used for high dynamic range (HDR) */
    GAMUT_MAP_HDR_EXPANSION = 3,      /**< Mapping expansion, used for HDR */
};

/**
 * @brief Enumerates the color space types.
 *
 */
enum ColorDataSpace {
    /** Unknown */
    COLOR_DATA_SPACE_UNKNOWN = 0,
    /** BT.601 color gamut */
    GAMUT_BT601 = 1,
    /** BT.709 color gamut */
    GAMUT_BT709 = 2,
    /** DCI_P3 color gamut */
    GAMUT_DCI_P3 = 3,
    /** SRGB color gamut */
    GAMUT_SRGB = 4,
    /** ADOBE_RGB color gamut */
    GAMUT_ADOBE_RGB = 5,
    /** DISPLAY_P3 color gamut */
    GAMUT_DISPLAY_P3 = 6,
    /** BT.2020 color gamut */
    GAMUT_BT2020 = 7,
    /** BT2100_PQ color gamut */
    GAMUT_BT2100_PQ = 8,
    /** BT2100_HLG color gamut */
    GAMUT_BT2100_HLG = 9,
    /** DISPLAY_BT2020 color gamut */
    GAMUT_DISPLAY_BT2020 = 10,
    /** Unspecified transform function */
    TRANSFORM_FUNC_UNSPECIFIED = 256,
    /** Linear transform function */
    TRANSFORM_FUNC_LINEAR = 512,
    /** SRGB transform function */
    TRANSFORM_FUNC_SRGB = 768,
    /** SMPTE_170M transform function */
    TRANSFORM_FUNC_SMPTE_170M = 1024,
    /** GM2_2 transform function */
    TRANSFORM_FUNC_GM2_2 = 1280,
    /** GM2_6 transform function */
    TRANSFORM_FUNC_GM2_6 = 1536,
    /** GM2_8 transform function */
    TRANSFORM_FUNC_GM2_8 = 1792,
    /** ST2084 transform function */
    TRANSFORM_FUNC_ST2084 = 2048,
    /** HLG transform function */
    TRANSFORM_FUNC_HLG = 2304,
    /** Unspecified precision */
    PRECISION_UNSPECIFIED = 65536,
    /** Full precision */
    PRECISION_FULL = 131072,
    /** Limited precision */
    PRESION_LIMITED = 196608,
    /** Extended precision */
    PRESION_EXTENDED = 262144,
    /** BT.601 color gamut | SMPTE_170M transform function | Full precision */
    BT601_SMPTE170M_FULL = GAMUT_BT601 | TRANSFORM_FUNC_SMPTE_170M | PRECISION_FULL,
    /** BT.601 color gamut | SMPTE_170M transform function | Limited precision */
    BT601_SMPTE170M_LIMITED = GAMUT_BT601 | TRANSFORM_FUNC_SMPTE_170M | PRESION_LIMITED,
    /** BT.709 color gamut | Linear transform function | Full precision */
    BT709_LINEAR_FULL = GAMUT_BT709 | TRANSFORM_FUNC_LINEAR | PRECISION_FULL,
    /** BT.709 color gamut | Linear transform function | Extended precision */
    BT709_LINEAR_EXTENDED = GAMUT_BT709 | TRANSFORM_FUNC_LINEAR | PRESION_EXTENDED,
    /** BT.709 color gamut | SRGB transform function | Full precision */
    BT709_SRGB_FULL = GAMUT_BT709 | TRANSFORM_FUNC_SRGB | PRECISION_FULL,
    /** BT.709 color gamut | SRGB transform function | Extended precision */
    BT709_SRGB_EXTENDED = GAMUT_BT709 | TRANSFORM_FUNC_SRGB | PRESION_EXTENDED,
    /** BT.709 color gamut | SMPTE_170M transform function | Limited precision */
    BT709_SMPTE170M_LIMITED = GAMUT_BT709 | TRANSFORM_FUNC_SMPTE_170M | PRESION_LIMITED,
    /** DCI_P3 color gamut | Linear transform function | Full precision */
    DCI_P3_LINEAR_FULL = GAMUT_DCI_P3 | TRANSFORM_FUNC_LINEAR | PRECISION_FULL,
    /** DCI_P3 color gamut | GM2_6 transform function | Full precision */
    DCI_P3_GAMMA26_FULL = GAMUT_DCI_P3 | TRANSFORM_FUNC_GM2_6 | PRECISION_FULL,
    /** DISPLAY_P3 color gamut | Linear transform function | Full precision */
    DISPLAY_P3_LINEAR_FULL = GAMUT_DISPLAY_P3 | TRANSFORM_FUNC_LINEAR | PRECISION_FULL,
    /** DCI_P3 color gamut | SRGB transform function | Full precision */
    DCI_P3_SRGB_FULL = GAMUT_DCI_P3 | TRANSFORM_FUNC_SRGB | PRECISION_FULL,
    /** ADOBE_RGB color gamut | GM2_2 transform function | Full precision */
    ADOBE_RGB_GAMMA22_FULL = GAMUT_ADOBE_RGB | TRANSFORM_FUNC_GM2_2 | PRECISION_FULL,
    /** BT.2020 color gamut | Linear transform function | Full precision */
    BT2020_LINEAR_FULL = GAMUT_BT2020 | TRANSFORM_FUNC_LINEAR | PRECISION_FULL,
    /** BT.2020 color gamut | SRGB transform function | Full precision */
    BT2020_SRGB_FULL = GAMUT_BT2020 | TRANSFORM_FUNC_SRGB | PRECISION_FULL,
    /** BT.2020 color gamut | SMPTE_170M transform function | Full precision */
    BT2020_SMPTE170M_FULL = GAMUT_BT2020 | TRANSFORM_FUNC_SMPTE_170M | PRECISION_FULL,
    /** BT.2020 color gamut | ST2084 transform function | Full precision */
    BT2020_ST2084_FULL = GAMUT_BT2020 | TRANSFORM_FUNC_ST2084 | PRECISION_FULL,
    /** BT.2020 color gamut | HLG transform function | Full precision */
    BT2020_HLG_FULL = GAMUT_BT2020 | TRANSFORM_FUNC_HLG | PRECISION_FULL,
    /** BT.2020 color gamut | ST2084 transform function | Limited precision */
    BT2020_ST2084_LIMITED = GAMUT_BT2020 | TRANSFORM_FUNC_ST2084 | PRESION_LIMITED,
};

/**
 * @brief Enumerates the HDR formats.
 *
 */
enum HDRFormat {
    NOT_SUPPORT_HDR = 0,        /**< HDR not supported */
    DOLBY_VISION = 1,           /**< Dolby Vision */
    HDR10 = 2,                  /**< HDR10 */
    HLG = 3,                    /**< HLG */
    HDR10_PLUS = 4,             /**< HDR10 Plus */
    HDR_VIVID = 5,              /**< Vivid */
};

/**
 * @brief Enumerates the HDR metadata keys.
 *
 */
enum HDRMetadataKey {
    MATAKEY_RED_PRIMARY_X = 0,                       /**< X coordinate of the red primary color */
    MATAKEY_RED_PRIMARY_Y = 1,                       /**< Y coordinate of the red primary color */
    MATAKEY_GREEN_PRIMARY_X = 2,                     /**< X coordinate of the green primary color */
    MATAKEY_GREEN_PRIMARY_Y = 3,                     /**< Y coordinate of the green primary color */
    MATAKEY_BLUE_PRIMARY_X = 4,                      /**< X coordinate of the blue primary color */
    MATAKEY_BLUE_PRIMARY_Y = 5,                      /**< Y coordinate of the blue primary color */
    MATAKEY_WHITE_PRIMARY_X = 6,                     /**< X coordinate of the white point */
    MATAKEY_WHITE_PRIMARY_Y = 7,                     /**< Y coordinate of the white point */
    MATAKEY_MAX_LUMINANCE = 8,                       /**< Maximum luminance */
    MATAKEY_MIN_LUMINANCE = 9,                       /**< Minimum luminance */
    MATAKEY_MAX_CONTENT_LIGHT_LEVEL = 10,            /**< Maximum content light level (MaxCLL) */
    MATAKEY_MAX_FRAME_AVERAGE_LIGHT_LEVEL = 11,      /**< Maximum frame average light level (MaxFALLL) */
    MATAKEY_HDR10_PLUS = 12,                         /**< HDR10 Plus */
    MATAKEY_HDR_VIVID = 13,                          /**< Vivid */
};

struct HdiBufferHandleInfo {
    unsigned int seqId;
    BufferHandleParcelable hdl;
};

struct HdifdInfo {
    int id;
    HdifdParcelable hdiFd;
};

/**
 * @brief Defines a property object that contains the property name, ID, and value.
 *
 */
struct PropertyObject {
    String name;             /**< Property name */
    unsigned int propId;     /**< Property ID */
    unsigned long value;     /**< Property value */
};

/**
 * @brief Defines the display capability.
 */
struct DisplayCapability {
    String name;                           /**< Display name */
    enum InterfaceType type;               /**< Display interface type */
    unsigned int phyWidth;                 /**< Physical width */
    unsigned int phyHeight;                /**< Physical width */
    unsigned int supportLayers;            /**< Number of supported layers */
    unsigned int virtualDispCount;         /**< Number of supported virtual displays */
    boolean supportWriteBack;              /**< Support for writeback */
    unsigned int propertyCount;            /**< Size of the property array */
    struct PropertyObject[] props;         /**< Property array */
};

/**
 * @brief Defines the display information.
 *
 */
struct DisplayInfo {
    unsigned int width;         /**< Display width */
    unsigned int height;        /**< Display height */
    int rotAngle;               /**< Rotation angle of the display */
};

/**
 * @brief Defines the layer information.
 *
 * <b>LayerInfo</b> must be passed to the {@link OpenLayer} interface, which creates a layer based on the layer information.
 *
 */
struct LayerInfo {
    int width;                        /**< Layer width */
    int height;                       /**< Layer height */
    enum LayerType type;              /**< Layer type, which can be a graphic layer, overlay layer, or sideband layer */
    int bpp;                          /**< Number of bits per pixel */
    enum PixelFormat pixFormat;       /**< Pixel format of the layer */
};

/**
 * @brief Defines the alpha information of a layer.
 *
 */
struct LayerAlpha {
    boolean enGlobalAlpha;    /**< Global alpha enable bit */
    boolean enPixelAlpha;     /**< Pixel alpha enable bit */
    unsigned char alpha0;     /**< Alpha-0 value, within the range [0, 255] */
    unsigned char alpha1;     /**< Alpha-1 value, within the range [0, 255] */
    unsigned char gAlpha;     /**< Global alpha value, within the range [0, 255] */
};

/**
 * @brief Defines the buffer data of a layer, including the virtual and physical memory addresses.
 *
 */
struct BufferData {
    unsigned long phyAddr;         /**< Physical memory address */
    unsigned long long virAddr;    /**< Virtual memory address */
};

/**
 * @brief Defines the buffer used to store layer data.
 *
 */
struct LayerBuffer {
    FileDescriptor fenceId;          /**< Fence ID of the buffer */
    int width;                       /**< Buffer width */
    int height;                      /**< Buffer height */
    int pitch;                       /**< Number of bytes from one row of pixels to the next row of pixels in memory */
    enum PixelFormat pixFormat;      /**< Pixel format of the buffer */
    struct BufferData data;          /**< Buffer data of the layer */
    BufferHandleParcelable hdl;      /**< Buffer handle of the layer */
};

/**
 * @brief Defines the information about a rectangle.
 *
 */
struct IRect {
    int x;    /**< Start X coordinate of the rectangle */
    int y;    /**< Start Y coordinate of the rectangle */
    int w;    /**< Width of the rectangle */
    int h;    /**< Height of the rectangle */
};

/**
 * @brief Defines surface information for hardware acceleration, such as image drawing and bit blit.
 */
struct ISurface {
    unsigned long phyAddr;             /**< Start physical address of the image */
    int height;                        /**< Image height */
    int width;                         /**< Image width */
    int stride;                        /**< Image stride */
    enum PixelFormat enColorFmt;       /**< Image format */
    boolean bYCbCrClut;                /**< Whether the color lookup table (CLUT) is in the YCbCr space */
    boolean bAlphaMax255;              /**< Maximum alpha value of the image (255 or 128) */
    boolean bAlphaExt1555;             /**< ARGB1555 alpha extension enable bit */
    unsigned char alpha0;              /**< Alpha-0 value, within the range [0,255] */
    unsigned char alpha1;              /**< Alpha-1 value, within the range [0,255] */
    unsigned long cbcrPhyAddr;         /**< CbCr component address */
    int cbcrStride;                    /**< CbCr component stride */
    unsigned long clutPhyAddr;         /**< Start physical address of the CLUT, used for color extension or correction */
};

/**
 * @brief Describes a line to help line drawing in hardware acceleration.
 *
 */
struct ILine {
    int x0;                         /**< X coordinate of the start point of the line */
    int y0;                         /**< Y coordinate of the start point of the line */
    int x1;                         /**< X coordinate of the end point of the line */
    int y1;                         /**< Y coordinate of the end point of the line */
    unsigned int color;             /**< Line color */
};

/**
 * @brief Describes a circle to help circle drawing in hardware acceleration.
 *
 */
struct ICircle {
    int x;                         /**< X coordinate of the circle center */
    int y;                         /**< Y coordinate of the circle center */
    int r;                         /**< Radius of the circle */
    unsigned int color;            /**< Color of the circle */
};

/**
 * @brief Describes a rectangle to help rectangle drawing in hardware acceleration.
 *
 */
struct Rectangle {
    struct IRect rect;            /**< Bounds of a rectangle */
    unsigned int color;           /**< Color of the rectangle */
};

/**
 * @brief Defines hardware acceleration options.
 *
 */
struct GfxOpt {
    boolean enGlobalAlpha;             /**< Global alpha enable bit */
    unsigned int globalAlpha;          /**< Global alpha value */
    boolean enPixelAlpha;              /**< Pixel alpha enable bit */
    enum BlendType blendType;          /**< Blending type */
    enum ColorKey colorKeyFrom;        /**< Color key mode */
    boolean enableRop;                 /**< ROP enable bit */
    enum RopType colorRopType;         /**< Color ROP type */
    enum RopType alphaRopType;         /**< Alpha ROP type */
    boolean enableScale;               /**< Scaling enable bit */
    enum TransformType rotateType;     /**< Rotation type */
    enum MirrorType mirrorType;        /**< Mirror type */
};

/**
 * @brief Defines the output mode information.
 */
struct DisplayModeInfo {
    int width;                   /**< Pixel width */
    int height;                  /**< Pixel height */
    unsigned int freshRate;      /**< Refresh rate */
    int id;                      /**< Mode ID */
};

/**
 * @brief Defines the HDR capability.
 *
 */
struct HDRCapability {
    unsigned int formatCount;        /**< Number of supported HDR formats */
    enum HDRFormat[] formats;        /**< Start address of the supported HDR format array */
    float maxLum;                    /**< Maximum luminance */
    float maxAverageLum;             /**< Maximum average luminance */
    float minLum;                    /**< Minimum luminance */
};

/**
 * @brief Defines the HDR metadata.
 *
 */
struct HDRMetaData {
    enum HDRMetadataKey key;        /**< HDR metadata key */
    float value;                    /**< Value corresponding to the key */
};

/**
 * @brief Enumerates the presentation timestamp (PTS) types.
 *
 */
enum PresentTimestampType {
    HARDWARE_DISPLAY_PTS_UNSUPPORTED = 0,        /**< Unsupported */
    HARDWARE_DISPLAY_PTS_DELAY = 1 << 0,         /**< Delay type */
    HARDWARE_DISPLAY_PTS_TIMESTAMP = 1 << 1,     /**< Timestamp type */
};

/**
 * @brief Defines the PTS.
 *
 */
struct PresentTimestamp {
    enum PresentTimestampType type;          /**< PTS type */
    long time;                               /**< Value corresponding to the PTS type */
};

/**
 * @brief Defines the extended data handle.
 *
 */
struct ExtDataHandle {
    int fd;                          /**< Handle FD, -1 if the handle is not supported */
    unsigned int reserveInts;        /**< Size of the reserved integer array */
    int[] reserve;                   /**< Reserved integer array */
};

/**
 * @brief Defines the YUV description information.
 *
 */
struct YUVDescInfo {
    unsigned long baseAddr;           /**< Base address of the memory */
    unsigned int yOffset;             /**< Y offset */
    unsigned int uOffset;             /**< U offset */
    unsigned int vOffset;             /**< V offset */
    unsigned int yStride;             /**< Y stride */
    unsigned int uvStride;            /**< UV stride */
    unsigned int uvStep;              /**< UV step */
};
/** @} */
