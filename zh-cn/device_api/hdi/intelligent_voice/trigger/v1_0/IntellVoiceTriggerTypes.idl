/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup IntelligentVoiceTrigger
 * @{
 *
 * @brief IntelligentVoiceTrigger模块向上层服务提供了统一接口。
 *
 * 上层服务开发人员可根据IntelligentVoiceTrigger模块提供的向上统一接口获取如下能力：触发器适配器加载卸载、智能语音触发器模型加载卸载、底层唤醒业务启动停止等。
 *
 * @since 4.0
 * @version 1.0
 */

/**
 * @file IntellVoiceTriggerTypes.idl
 *
 * @brief IntelligentVoiceTrigger模块接口定义中使用的数据类型，包括智能语音触发器模型类型、识别状态、触发器适配器描述符、驱动属性、模型信息、识别事件信息等。
 *
 * 模块包路径：ohos.hdi.intelligent_voice.trigger.v1_0
 *
 * @since 4.0
 * @version 1.0
 */


package ohos.hdi.intelligent_voice.trigger.v1_0;

/**
 * @brief 智能语音触发器模型类型。
 * 
 * @since 4.0
 * @version 1.0
 */
enum IntellVoiceTriggerModelType {
    /** 未知模型 */
    UNKNOWN = -1,
    /** 默认模型 */
    DEFAULT = 1,
};

/**
 * @brief 识别状态。
 * 
 * @since 4.0
 * @version 1.0
 */
enum RecognitionStatus {
    /** 识别成功 */
    SUCCESS = 0,
    /** 识别中止 */
    ABORT = 1,
    /** 识别失败 */
    FAILURE = 2,
};

/**
 * @brief 智能语音触发器适配器描述符。
 * 
 * @since 4.0
 * @version 1.0
 */
struct IntellVoiceTriggerAdapterDsecriptor {
    /** 适配器名称 */
    String adapterName;
};

/**
 * @brief 智能语音触发器属性。
 * 
 * @since 4.0
 * @version 1.0
 */
struct IntellVoiceTriggerProperties {
    /** 触发器名称 */
    String implementor;
    /** 触发器描述 */
    String description;
    /** 触发器版本号 */
    unsigned int version;
    /** 触发器支持最大可加载模型数 */
    unsigned int maxIntellVoiceModels;
};

/**
 * @brief 智能语音触发器模型信息。
 * 
 * @since 4.0
 * @version 1.0
 */
struct IntellVoiceTriggerModel {
    /** 智能语音触发器模型类型 */
    enum IntellVoiceTriggerModelType type;
    /** 智能语音触发器模型标识 */
    unsigned int uid;
    /** 智能语音触发器模型内容 */
    Ashmem data;
};

/**
 * @brief 智能语音识别事件信息。
 * 
 * @since 4.0
 * @version 1.0
 */
struct IntellVoiceRecognitionEvent {
    /** 识别状态 */
    enum RecognitionStatus status;
    /** 智能语音触发器模型类型 */
    enum IntellVoiceTriggerModelType type;
    /** 智能语音触发器模型句柄 */
    int  modelHandle;
};
/** @} */