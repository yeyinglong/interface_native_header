/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup IntelligentVoiceTrigger
 * @{
 *
 * @brief IntelligentVoiceTrigger模块向上层服务提供了统一接口。
 *
 * 上层服务开发人员可根据IntelligentVoiceTrigger模块提供的向上统一接口获取如下能力：触发器适配器加载卸载、智能语音触发器模型加载卸载、底层唤醒业务启动停止等。
 *
 * @since 4.0
 * @version 1.0
 */

/**
 * @file IIntellVoiceTriggerAdapter.idl
 *
 * @brief IntelligentVoiceTrigger模块触发器适配器接口，包括获取智能语音触发器属性、加载卸载智能语音触发器模型、启动停止底层唤醒业务等。
 *
 * 模块包路径：ohos.hdi.intelligent_voice.trigger.v1_0
 *
 * 引用：
 * - ohos.hdi.intelligent_voice.trigger.v1_0.IntellVoiceTriggerTypes
 * - ohos.hdi.intelligent_voice.trigger.v1_0.IIntellVoiceTriggerCallback
 *
 * @since 4.0
 * @version 1.0
 */

package ohos.hdi.intelligent_voice.trigger.v1_0;

import ohos.hdi.intelligent_voice.trigger.v1_0.IntellVoiceTriggerTypes;
import ohos.hdi.intelligent_voice.trigger.v1_0.IIntellVoiceTriggerCallback;

 /**
 * @brief IntelligentVoiceTrigger模块向上层服务提供了智能语音触发器适配器接口。
 *
 * 上层服务开发人员可根据IntelligentVoiceTrigger模块提供的向上智能语音触发器适配器接口实现获取智能语音触发器属性、加载卸载智能语音触发器模型、启动停止底层唤醒业务等功能。
 *
 * @since 4.0
 * @version 1.0
 */
interface IIntellVoiceTriggerAdapter {
    /**
     * @brief 获取智能语音触发器属性。
     *
     * @param properties 智能语音触发器属性，信息包含触发器名称、描述、版本、支持最大模型数，具体参考{@link IntellVoiceTriggerProperties}。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    GetProperties([out] struct IntellVoiceTriggerProperties properties);
    /**
     * @brief 加载模型。
     *
     * @param model 智能语音触发器模型信息，信息包含类型、标识、内容，具体参考{@link IntellVoiceTriggerModel}。
     * @param triggerCallback 触发器回调接口，具体参考{@link IIntellVoiceTriggerCallback}。
     * @param cookie 上层调用者标识。
     * @param handle 返回给上层的模型句柄。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    LoadModel([in] struct IntellVoiceTriggerModel model, [in] IIntellVoiceTriggerCallback triggerCallback, [in] int cookie, [out] int handle);
    /**
     * @brief 卸载模型。
     *
     * @param handle 智能语音触发器模型句柄。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    UnloadModel([in] int handle);
    /**
     * @brief 启动底层唤醒算法。
     *
     * @param handle 智能语音触发器模型句柄。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    Start([in] int handle);
    /**
     * @brief 停止底层唤醒算法。
     *
     * @param handle 智能语音触发器模型句柄。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    Stop([in] int handle);
}
/** @} */