/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup Camera
 * @{
 *
 * @brief Camera模块接口定义。
 *
 * Camera模块涉及相机设备的操作、流的操作、离线流的操作和各种回调等。
 *
 * @since 4.0
 * @version 1.1
 */

/**
 * @file ICameraDevice.idl
 *
 * @brief Camera设备操作接口。
 *
 * 模块包路径：ohos.hdi.camera.v1_1
 *
 * 引用：
 * - ohos.hdi.camera.v1_0.ICameraDevice
 * - ohos.hdi.camera.v1_0.IStreamOperatorCallback
 * - ohos.hdi.camera.v1_1.IStreamOperator
 * - ohos.hdi.camera.v1_1.Types
 *
 * @since 4.0
 * @version 1.1
 */

package ohos.hdi.camera.v1_1;

import ohos.hdi.camera.v1_0.ICameraDevice;
import ohos.hdi.camera.v1_0.IStreamOperatorCallback;
import ohos.hdi.camera.v1_1.IStreamOperator;
import ohos.hdi.camera.v1_1.Types;

/**
 * @brief 定义Camera设备基本的操作。
 *
 * 设置流回调接口、更新控制参数、执行metadata相关操作。
 *
 * @since 4.0
 * @version 1.1
 */
interface ICameraDevice extends ohos.hdi.camera.v1_0.ICameraDevice {
    /**
     * @brief 获取流操作句柄。
     *
     * @param callbackObj 设置流回调接口，详细可查看{@link IStreamOperatorCallback}，
     * 用于上报捕获开始{@link OnCaptureStarted}，捕获结束{@link OnCaptureEnded}，
     * 捕获错误等信息{@link OnCaptureError}。
     * @param streamOperator 返回流操作句柄。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link CamRetCode}。
     *
     * @since 4.0
     * @version 1.1
     */
    GetStreamOperator_V1_1([in] IStreamOperatorCallback callbackObj, [out] IStreamOperator streamOperator);

    /**
     * @brief 获取默认的相机设备控制参数。
     *
     * @param settings 指示默认的相机参数, 包括传感器帧速率和3A参数。
     *
     * 3A 代表自动对焦 (AF), 自动曝光 (AE), 和自动白平衡 (?AWB)。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link CamRetCode}。
     *
     * @since 4.0
     * @version 1.1
     */
    GetDefaultSettings([out] unsigned char[] settings);
}
/** @} */