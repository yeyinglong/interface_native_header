/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdiNfc
 * @{
 *
 * @brief 为nfc服务提供统一的访问nfc驱动的接口。
 *
 * NFC服务通过获取的nfc驱动对象提供的API接口访问nfc驱动，包括开关NFC、初始化NFC、读写数据、配置RF参数、
 * 通过IO控制发送NCI指令给nfc驱动。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file NfcTypes.idl
 *
 * @brief 声明类型定义，包括开关NFC、初始化NFC、读写数据、配置RF参数等。
 *
 * 模块包路径：ohos.hdi.nfc.v1_0
 *
 * @since 3.2
 * @version 1.0
 */

package ohos.hdi.nfc.v1_0;

/**
 * @brief NFC事件（包括打开NFC完成、关闭NFC完成、预配置NFC完成等上报事件）的枚举定义。
 *
 * @since 3.2
 */
enum NfcEvent {
    /** NFC打开完成事件 */
    OPEN_CPLT           = 0,
    /** NFC关闭完成事件 */
    CLOSE_CPLT          = 1,
    /** NFC初始化完成事件 */
    POST_INIT_CPLT      = 2,
    /** NFC discover预配置完成事件 */
    PRE_DISCOVER_CPLT   = 3,
    /** 请求控制事件 */
    REQUEST_CONTROL     = 4,
    /** 释放控制事件 */
    RELEASE_CONTROL     = 5,
    /** 错误事件 */
    ERROR               = 6,
    /** HCI复位事件 */
    HCI_NETWORK_RESET   = 7,
};

/**
 * @brief NFC状态的枚举定义。
 *
 * @since 3.2
 */
enum NfcStatus {
    /** NFC状态OK */
    OK               = 0,
    /** NFC状态失败 */
    FAILED           = 1,
    /** 传输错误 */
    ERR_TRANSPORT    = 2,
    /** 发送命令超时 */
    ERR_CMD_TIMEOUT  = 3,
    /** 请求被拒绝 */
    REFUSED          = 4,
};

/**
 * @brief NFC指令的枚举定义。
 *
 * @since 3.2
 */
enum NfcCommand {
    /** 无效指令 */
    CMD_INVALID = 0,
};
/** @} */